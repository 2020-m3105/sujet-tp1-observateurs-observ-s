package fr.univlille.iutinfo.m3105.q1;


public abstract class Subject {

	protected void notifyObervers() {
	}

	protected void notifyObervers(Object data) {
	}

	public void attach(Observer observer) {
	}

	public void detach(Observer observer) {
	}
}
