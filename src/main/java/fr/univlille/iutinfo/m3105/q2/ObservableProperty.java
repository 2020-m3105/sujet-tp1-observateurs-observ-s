package fr.univlille.iutinfo.m3105.q2;

import fr.univlille.iutinfo.m3105.q1.Observer;

public class ObservableProperty {

	public void setValue(Object i) {
	}

	public Object getValue() {
		return null;
	}

	public void attach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
	}

	public void detach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
	}

}
